# scatterometer_virtual_xpsc8

Simulates the XPS-C8 controller.  It exists to enable development while the physical controller is not available.