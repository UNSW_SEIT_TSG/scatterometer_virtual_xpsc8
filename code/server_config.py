"""Stores variable values that are common between other modules in the repo."""

xpsc8_port = 5001
host = ''
current_position = 0.0
listen_count = 500
server_address = "131.236.55.37"
server_buffer_length = 2048

def printVariables(variable_names):
    """Renders variables and their values on the terminal."""
    max_name_len = max([len(k) for k in variable_names])
    max_val_len = max([len(str(globals()[k])) for k in variable_names])

    for k in variable_names:
        print(f'  {k:<{max_name_len}}:  {globals()[k]:>{max_val_len}}')

if __name__ == "__main__":
    print(__doc__)
    ks = [k for k in dir() if (k[:2] != "__" and not callable(globals()[k]))]
    printVariables(ks)
