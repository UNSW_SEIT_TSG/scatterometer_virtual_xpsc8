"""This module simulates the XPS-C8 controller.  It exists so development can
continue when the physical controller is not available."""

import socket
from _thread import start_new_thread

from server_config import (
    xpsc8_port,
    host,
    current_position,    # Model State
    listen_count,
    server_buffer_length
)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.bind((host, xpsc8_port))
except socket.error as e:
    print(str(e))

s.listen(listen_count)

def determineReply(received_string: str, stack_state):
    """Selects the appropriate response given the inbound command.
    Also manages the state of the current position."""
    #todo need to handle state of all stages rather than just one.

    current_pos = stack_state['current_position']
    received_string_split = received_string.split("(")
    received_string_command_name = received_string_split[0]
    received_string_split[-1] = received_string_split[-1].replace(")", "")
    received_string_parameters = received_string_split[-1].split(",")
    if received_string_command_name == "GroupMoveAbsolute":
        current_pos = received_string_parameters[-1]
    print(received_string_command_name)

    command_response_dict = {
        "CloseAllOtherSockets":                                 "0,",
        "ControllerMotionKernelTimeLoadGet":                    "0,",
        "ControllerStatusGet":                                  "0,0",
        "ControllerStatusStringGet":                            "0,",
        "DoubleGlobalArrayGet":                                 "0,",
        "DoubleGlobalArraySet":                                 "0,",
        "ElapsedTimeGet":                                       "0,",
        "ErrorStringGet":                                       "0,",
        "EventExtendedAllGet":                                  "0,",
        "EventExtendedConfigurationActionGet":                  "0,",
        "EventExtendedConfigurationActionSet":                  "0,",
        "EventExtendedConfigurationTriggerGet":                 "0,",
        "EventExtendedConfigurationTriggerSet":                 "0,",
        "EventExtendedGet":                                     "0,",
        "EventExtendedRemove":                                  "0,",
        "EventExtendedStart":                                   "0,",
        "EventExtendedWait":                                    "0,",
        "FirmwareVersionGet":                                   "0,",
        "GatheringConfigurationGet":                            "0,",
        "GatheringConfigurationSet":                            "0,",
        "GatheringCurrentNumberGet":                            "0,",
        "GatheringDataAcquire":                                 "0,",
        "GatheringDataGet":                                     "0,",
        "GatheringDataMultipleLinesGet":                        "0,",
        "GatheringExternalConfigurationGet":                    "0,",
        "GatheringExternalConfigurationSet":                    "0,",
        "GatheringExternalCurrentNumberGet":                    "0,",
        "GatheringExternalDataGet":                             "0,",
        "GatheringExternalStopAndSave":                         "0,",
        "GatheringReset":                                       "0,",
        "GatheringRunAppend":                                   "0,",
        "GatheringRun":                                         "0,",
        "GatheringStop":                                        "0,",
        "GatheringStopAndSave":                                 "0,",
        "GlobalArrayGet":                                       "0,",
        "GlobalArraySet":                                       "0,",
        "GPIOAnalogGainGet":                                    "0,",
        "GPIOAnalogGainSet":                                    "0,",
        "GPIOAnalogGet":                                        "0,",
        "GPIOAnalogSet":                                        "0,",
        "GPIODigitalGet":                                       "0,",
        "GPIODigitalSet":                                       "0,",
        "GroupAccelerationSetpointGet":                         "0,",
        "GroupAnalogTrackingModeDisable":                       "0,",
        "GroupAnalogTrackingModeEnable":                        "0,",
        "GroupCorrectorOutputGet":                              "0,",
        "GroupCurrentFollowingErrorGet":                        "0,",
        "GroupHomeSearch":                                      "0,",
        "GroupHomeSearchAndRelativeMove":                       "0,",
        "GroupInitialize":                                      "0,",
        "GroupInitializeWithEncoderCalibration":                "0,",
        "GroupJogCurrentGet":                                   "0,",
        "GroupJogModeDisable":                                  "0,",
        "GroupJogModeEnable":                                   "0,",
        "GroupJogParametersGet":                                "0,",
        "GroupJogParametersSet":                                "0,",
        "GroupKill":                                            "0,",
        "GroupMotionDisable":                                   "0,",
        "GroupMotionEnable":                                    "0,",
        "GroupMoveAbort":                                       "0,",
        "GroupMoveAbsolute":                                    "0,",
        "GroupMoveRelative":                                    "0,",
        "GroupPositionCorrectedProfilerGet":                    "0,",
        "GroupPositionCurrentGet":                              "0,"+str(current_pos),
        "GroupPositionPCORawEncoderGet":                        "0,",
        "GroupPositionSetpointGet":                             "0,",
        "GroupPositionTargetGet":                               "0,",
        "GroupReferencingActionExecute":                        "0,",
        "GroupReferencingStart":                                "0,",
        "GroupReferencingStop":                                 "0,",
        "GroupStatusGet":                                       "0,",
        "GroupStatusStringGet":                                 "0,",
        "GroupVelocityCurrentGet":                              "0,",
        "HardwareDateAndTimeGet":                               "0,",
        "HardwareDateAndTimeSet":                               "0,",
        "KillAll":                                              "0,",
        "Login":                                                "0,",
        "PositionerAccelerationAutoScaling":                    "0,",
        "PositionerAnalogTrackingPositionParametersGet":        "0,",
        "PositionerAnalogTrackingPositionParametersSet":        "0,",
        "PositionerAnalogTrackingVelocityParametersGet":        "0,",
        "PositionerAnalogTrackingVelocityParametersSet":        "0,",
        "PositionerBacklashDisable":                            "0,",
        "PositionerBacklashEnable":                             "0,",
        "PositionerBacklashGet":                                "0,",
        "PositionerBacklashSet":                                "0,",
        "PositionerCorrectorAutoTuning":                        "0,",
        "PositionerCorrectorNotchFiltersGet":                   "0,",
        "PositionerCorrectorNotchFiltersSet":                   "0,",
        "PositionerCorrectorPIDDualFFVoltageGet":               "0,",
        "PositionerCorrectorPIDDualFFVoltageSet":               "0,",
        "PositionerCorrectorPIDFFAccelerationGet":              "0,",
        "PositionerCorrectorPIDFFAccelerationSet":              "0,",
        "PositionerCorrectorPIDFFVelocityGet":                  "0,",
        "PositionerCorrectorPIDFFVelocitySet":                  "0,",
        "PositionerCorrectorPIPositionGet":                     "0,",
        "PositionerCorrectorPIPositionSet":                     "0,",
        "PositionerCorrectorTypeGet":                           "0,",
        "PositionerCurrentVelocityAccelerationFiltersGet":      "0,",
        "PositionerCurrentVelocityAccelerationFiltersSet":      "0,",
        "PositionerDriverFiltersGet":                           "0,",
        "PositionerDriverFiltersSet":                           "0,",
        "PositionerDriverPositionOffsetsGet":                   "0,",
        "PositionerDriverStatusGet":                            "0,",
        "PositionerDriverStatusStringGet":                      "0,",
        "PositionerEncoderAmplitudeValuesGet":                  "0,",
        "PositionerEncoderCalibrationParametersGet":            "0,",
        "PositionerErrorGet":                                   "0,",
        "PositionerErrorRead":                                  "0,",
        "PositionerErrorStringGet":                             "0,",
        "PositionerExcitationSignalGet":                        "0,",
        "PositionerExcitationSignalSet":                        "0,",
        "PositionerExternalLatchPositionGet":                   "0,",
        "PositionerHardInterpolatorFactorGet":                  "0,",
        "PositionerHardInterpolatorFactorSet":                  "0,",
        "PositionerHardwareStatusGet":                          "0,",
        "PositionerHardwareStatusStringGet":                    "0,",
        "PositionerMaximumVelocityAndAccelerationGet":          "0,",
        "PositionerMotionDoneGet":                              "0,",
        "PositionerMotionDoneSet":                              "0,",
        "PositionerPositionCompareAquadBAlwaysEnable":          "0,",
        "PositionerPositionCompareAquadBWindowedGet":           "0,",
        "PositionerPositionCompareAquadBWindowedSet":           "0,",
        "PositionerPositionCompareDisable":                     "0,",
        "PositionerPositionCompareEnable":                      "0,",
        "PositionerPositionCompareGet":                         "0,",
        "PositionerPositionComparePulseParametersGet":          "0,",
        "PositionerPositionComparePulseParametersSet":          "0,",
        "PositionerPositionCompareSet":                         "0,",
        "PositionerRawEncoderPositionGet":                      "0,",
        "PositionersEncoderIndexDifferenceGet":                 "0,",
        "PositionerSGammaExactVelocityAjustedDisplacementGet":  "0,",
        "PositionerSGammaParametersGet":                        "0,",
        "PositionerSGammaParametersSet":                        "0,",
        "PositionerSGammaPreviousMotionTimesGet":               "0,",
        "PositionerStageParameterGet":                          "0,",
        "PositionerStageParameterSet":                          "0,",
        "PositionerTimeFlasherDisable":                         "0,",
        "PositionerTimeFlasherEnable":                          "0,",
        "PositionerTimeFlasherGet":                             "0,",
        "PositionerTimeFlasherSet":                             "0,",
        "PositionerUserTravelLimitsGet":                        "0,",
        "PositionerUserTravelLimitsSet":                        "0,",
        "Reboot":                                               "0,",
        "SingleAxisSlaveModeDisable":                           "0,",
        "SingleAxisSlaveModeEnable":                            "0,",
        "SingleAxisSlaveParametersGet":                         "0,",
        "SingleAxisSlaveParametersSet":                         "0,",
        "TCLScriptExecuteAndWait":                              "0,",
        "TCLScriptExecute":                                     "0,",
        "TCLScriptExecuteWithPriority":                         "0,",
        "TCLScriptKill":                                        "0,",
        "TimerGet":                                             "0,",
        "TimerSet":                                             "0,",
    }

    stack_state['current_position'] = current_pos

    if received_string_command_name in command_response_dict.keys():
        return command_response_dict[received_string_command_name]+",EndOfAPI", stack_state
    else:
        return "-7,"+received_string+",EndOfAPI", stack_state

def threaded_client(konn):
    """This function allows for the server behaviour to run as a separate thread
    to the main program.
    """
    stack_state = {'current_position': current_position}
    while True:
        data = konn.recv(server_buffer_length)
        reply, stack_state = determineReply(data.decode('utf-8'), stack_state)
        if not data:
            break
        konn.sendall(str.encode(reply))
    konn.close()

if __name__ == "__main__":
    while True:
        conn, addr = s.accept()
        print("connected to: "+addr[0]+";"+str(addr))

        start_new_thread(threaded_client, (conn, ))
